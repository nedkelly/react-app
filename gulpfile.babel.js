
import babelify   from "babelify";
import bourbon    from "node-bourbon";
import browserify from "browserify";
import bs         from "browser-sync";
import buf        from "gulp-buffer";
import cache      from "gulp-cache";
import combine    from "gulp-combine-mq";
import conf       from "./config.js";
import cssmin     from "gulp-cssmin";
import del        from "del";
import gulp       from "gulp";
import header     from "gulp-header";
import imagemin   from "gulp-imagemin";
import eslint     from "gulp-eslint";
import notify     from "gulp-notify";
import pkg        from "./package.json";
import plumber    from "gulp-plumber";
import ps         from "project-server";
import rename     from "gulp-rename";
import sass       from "gulp-sass";
import tap        from "gulp-tap";
import uglify     from "gulp-uglify";
import utility    from "./utility.js";
import watch      from "gulp-watch";

process.env.PROJECT_ENV = "dev";

// CSS
gulp.task("css", () => {
  return gulp.src(conf.css.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(sass({ outputStyle: "expanded", includePaths: bourbon.includePaths }).on("error", sass.logError))
    .pipe(combine({ beautify: false }))
    .pipe(cssmin())
    .pipe(header(conf.banner, { pkg: pkg, timestamp: Math.floor(Date.now() / 1000) }))
    .pipe(gulp.dest(conf.css.output))
    .pipe(notify({ title: "CSS", message: "✓ CSS task complete", onLast: true }));
});

// JS
gulp.task("js", ["js:lint"], () => {
  return gulp.src(conf.js.input, { read: false })
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(tap((file) => {
      file.contents = browserify(file.path, {
        debug: true,
        extensions: [".js", ".jsx"]
      })
      .transform(babelify.configure({
        presets: ["es2015", "react"],
        extensions: [".js", ".jsx"]
      }))
      .transform("envify", {
        PROJECT_ENV: process.env.PROJECT_ENV
      })
      .bundle()
      .on("error", function (err) {
        utility.errorHandler(err, this);
        this.emit("end");
      });
    }))
    .pipe(buf())
    .pipe(uglify())
    .pipe(header(conf.banner, { pkg: pkg, timestamp: Math.floor(Date.now() / 1000) }))
    .pipe(rename("app.js"))
    .pipe(gulp.dest(conf.js.output))
    .pipe(notify({ title: "JS", message: "✓ JS task complete", onLast: true }));
});

// JS Lint
gulp.task("js:lint", () => {
    return gulp.src(conf.js.app)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(eslint({
      fix: true
    }))
    .pipe(eslint.format("codeframe"));
});

// Images
gulp.task("images", () => {
  return gulp.src(conf.images.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest(conf.images.output))
    .pipe(notify({ title: "Images", message: "✓ Images task complete", onLast: true }));
});

// HTML
gulp.task("html", () => {
  return gulp.src(conf.html.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(gulp.dest(conf.html.output))
    .pipe(notify({ title: "HTML", message: "✓ HTML task complete", onLast: true }));
});

// Serve
gulp.task("serve", () => {
  bs.init({
    files: [conf.server.output],
    reloadDebounce: 1000,
    server: {
      baseDir: conf.server.input,
      directory: true,
      middleware: [ps.projectServerIndex(conf.server.input, { config: conf.server.projectServer })],
    },
    notify: false,
    online: false,
    open: "local"
  });
});

// Styleguide
gulp.task("styleguide", () => {
  return ps.projectServerStyleguide(conf.server.input, { config: conf.server.projectServer });
});

// Watch
gulp.task("watch", () => {
  conf.watch.tasks.forEach((t) => {
    watch(t.path, () => {
      gulp.start(t.task);
    });
  });
});

// Clean
gulp.task("clean", (cb) => {
  return del([conf.clean.input], cb);
});

// Default Task
gulp.task("default", ["css", "js", "images", "html", "serve", "watch"]);

// Prod Build
gulp.task("prod", () => {
   process.env.PROJECT_ENV = "prod";
   gulp.start(["css", "js", "images", "html"]);
});
