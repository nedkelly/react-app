# React Start App

> This is a basic Gulp, Browserify/Babelify [React App](https://facebook.github.io/react/) with SASS with Bourbon.

## Getting Started

1. Clone the repository

```
git clone git@bitbucket.org:nedkelly/react-app.git
cd react-app
```

2. Install dependencies

```
npm i -g gulp && npm i
```

3. Run the default gulp task

```
gulp
```

## Tasks
- `gulp` - runs `css`, `js`, `images`, `html`, `serve` & `watch` tasks
- `gulp clean` - cleans the dest directory
- `gulp css` - compiles SASS from `src` to `dest`
- `gulp js` - compiles JavaScript from `src` to `dest`
- `gulp images` - compiles Images from `src` to `dest`
- `gulp html` - compiles HTML from `src` to `dest`
- `gulp prod` - runs `css`, `js`, `images` & `html` tasks
- `gulp serve` - runs Project Server
- `gulp styleguide` - Generates static styleguide
