
import pkg from "./package.json";

// Clean
module.exports.clean = {
  input: "./dest"
};

// CSS
module.exports.css = {
  input: ["./src/assets/css/**/*.scss"],
  output: "./dest/assets/css/"
};

// HTML
module.exports.html = {
  input: ["./src/**/*.html"],
  output: "./dest/"
};

// JS
module.exports.js = {
  app:   ["./src/app.jsx", "./src/app/**/*.jsx"],
  input: ["./src/app.jsx"],
  output: "./dest/assets/js/"
};

// Images
module.exports.images = {
  input: ["./src/assets/images/**/*"],
  output: "./dest/assets/images/"
};

// Server
module.exports.server = {
  input: "./dest/",
  output: "./dest/**",
  projectServer: {
    defaultPage: "/index.html",
    // favicon: "favicon.ico",
    localStorageSeed: "React Start App",
    publicDir: "./dest/",
    relativeDir: "",
    styleguidePage: "styleguide.html",
    theme: {
      sidebar: "dark"
    },
    menus: [
      { title: "Pages", path: "/", accesskey: "g" },
    ],
    transforms: "^(css|js|[ml+0-9]{5})",
    pkg: pkg
  }
};

// Watch
module.exports.watch = {
  tasks: [
    { path: module.exports.js.app,       task: ["js"] },
    { path: module.exports.css.input,    task: ["css"] },
    { path: module.exports.images.input, task: ["images"] },
    { path: module.exports.html.input,   task: ["html"] }
  ]
};

// Banner
module.exports.banner = [
  "/**",
  " * <%= pkg.name %>",
  " * @version v<%= pkg.version %>",
  " * @timestamp <%= timestamp %>",
  " */",
  ""
].join("\n");
