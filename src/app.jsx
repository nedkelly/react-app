import React    from "react";
import ReactDOM from "react-dom";
import Welcome  from "./app/welcome.component.jsx";

ReactDOM.render(
  <Welcome />,
  document.getElementById("app")
);
