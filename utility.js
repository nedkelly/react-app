
"use strict";

const args       = require("yargs").argv;
import chalk     from "chalk";
import gutil     from "gulp-util";
import mapStream from "map-stream";

/**
 * Whenever any plugin throws an error, this function will handle it.
 * @param  {Object} err
 * @return {Void}
 */
module.exports.errorHandler = (err) => {
  gutil.log(chalk.red(`Whoops, something has gone horribly wrong!\n\n${chalk.yellow(err)}\n\n`));
};

/**
 * Get Time Stamp
 * @return {String}
 */
module.exports.getTimeStamp = () => {
  let d = new Date();
  return ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2) + ":" + ("0" + d.getSeconds()).slice(-2);
};

/**
 * jshintReporter
 * @param  {String}   file
 * @param  {Function} cb
 * @return {Void}
 */
module.exports.jshintReporter = (file, cb) => {
  return mapStream((file, cb) => {
    let status = file.jshint.success ? chalk.green("✓ PASS"): chalk.red("✗ FAIL");
    if (!file.jshint.success) {
      gutil.log(`[${chalk.gray(module.exports.getTimeStamp())}] ${status} ${chalk.blue(file.path)}`);
      file.jshint.results.forEach((err) => {
        if (err) {
          gutil.log(`[${chalk.gray(module.exports.getTimeStamp())}] ${chalk.red(`✗`)} ${err.error.reason} on: line ${err.error.line}, col ${err.error.character}.`);
        }
      });
    } else {
      if (args.verbose) {
        gutil.log(`[${chalk.gray(module.exports.getTimeStamp())}] ${status} ${chalk.blue(file.path)}`);
      }
    }
    cb(null, file);
  });
};

module.exports.eslintReporter = (result) => {
  console.log(result);
};
